import openpyxl
import tkinter as tk
from tkinter import *
from openpyxl import Workbook
import datetime
from openpyxl import load_workbook
import numpy as np
wb = Workbook()
ws = wb.active
root = tk.Tk()
groot_brood=0
klein_brood=0
sandwich=0

class Application(tk.Frame):
	dict_prijzen = {}
	prijs=[2.0,1.4,1.2]
	product=['Groot brood', 'Klein brood', 'Sandwich']
	aantal=[0,0,0]
	producten=np.array(product)
	prijzen=np.array(prijs)
	aantallen=np.array(aantal)
	
	def __init__(self, master=None):
		super().__init__(master)
		self.pack()
		self.create_widgets()
		
	def create_widgets(self):
        #button1
		self.nieuw_maken = tk.Button(self) 
		self.nieuw_maken["text"] = "Nieuwe bestelbon"
		self.nieuw_maken["command"] = self.aanmaken
		self.nieuw_maken.pack()
		#button2
		self.open_overzicht=tk.Button(self)
		self.open_overzicht['text']='Bekijk het overzicht'
		self.open_overzicht['command']=self.overzicht
		self.open_overzicht.pack()
		#button3
		self.hoofd=tk.Button(self)
		self.hoofd['text']='Maak een nieuwe hoofdbestelbon aan'
		self.hoofd['command']=self.hoofd123
		self.hoofd.pack()

		self.quit = tk.Button(self, text="QUIT", fg="red",command=root.destroy)
		self.quit.pack(side="bottom")
	def aanmaken(self):
		print('aanmaken')
		Aanmaken.aanmaken_bon(self)
	
	def overzicht(self):
		print('overzicht')
		Overzicht.maak_overzicht(self)
		
	def hoofd123(self):
		print('hoofd')
		
class Overzicht(Application):
		def __init__(self):
			self.maak_overzicht()

		def printtext(self):
			print('overzicht2')

		def maak_overzicht(self):
			print('andere class')
			
			tabo=tk.Toplevel(self)
			tabo.wm_title("Overzicht")
			lo=tk.Label(tabo, text='Overzicht, label')
			lo.pack(side='top', fill='both', expand=True) 
			self.buttono=tk.Button(tabo)
			self.buttono['text']='Click me'
			self.buttono['command']=lambda : Overzicht.printtext(self)
			self.buttono.pack()

class Aanmaken(Application):
	
	def __init__(self):
		taba=tk.Toplevel(self)
		la=tk.Label(taba, text='Maak een nieuwe bestelbon aan')
		la.pack(side='top', fill='both',expand=True)
		self.e=Entry(taba)
		self.f=e.get()
		e.pack(side='bottom')
		self.buttona=tk.Button(taba,text='Maak aan', command=Aanmaken.aanmaken(self))
		buttona.pack(side='bottom')
		
	def aanmaken_bon(self):	
		#nieuwe bestelbon, naam van de klant
		self.taba=tk.Toplevel(self)
		self.la=tk.Label(self.taba, text='Wat is de naam van de klant?')
		self.la.pack(side='top', fill='both',expand=True)
		self.klant=Entry(self.taba)

		
		self.klant.pack(side='top')
		self.input=self.klant.get()
		print('input')
		print(self.klant.get())
		self.buttona=tk.Button(self.taba,text='Maak aan', command=lambda:Aanmaken.aanmaken(self))
		self.buttona.pack(side='bottom')
		
	def aanmaken(self):
		#aanmaken nieuwe canvas
		self.tabk=tk.Toplevel(self)
		self.tabk.wm_title(self.input)
		#waarden resetten
		self.aantallen[0:] = 0
		self.d={}
		#labels en buttons
		for row in range(len(self.producten)):
			label = tk.Label(self.tabk, text=self.producten[row])
			entry = tk.Entry(self.tabk)
			self.d[(row, 1)] = entry
			self.button = tk.Button(self.tabk)
			self.button['text'] = 'save'
			self.button['command'] = lambda row=row: Aanmaken.value(self, row)
			aantal_label = StringVar()
			aantal_label.set(str(self.aantallen[row]))
			label2 = tk.Label(self.tabk, textvariable=aantal_label)
			label.grid(row=row, column=0)
			entry.grid(row=row, column=1)
			self.button.grid(row=row, column=2)
			label2.grid(row=row, column=3)
		#button voor opslaan van waarden
		self.next_button=tk.Button(self.tabk)
		self.next_button['text']='Next'
		self.next_button['command']=lambda : Aanmaken.next(self)
		self.next_button.grid(row=len(self.producten),column=1)
		self.save_button=tk.Button(self.tabk)
		self.save_button['text']='Save'
		self.save_button['command']=lambda: Aanmaken.save(self)
		self.save_button.grid(row=len(self.producten), column=0)

	def value(self, val):
		print(val)
		var = self.d[(val, 1)].get()
		self.aantallen[val] = var
		aantal_label = StringVar()
		aantal_label.set(var)
		label3 = tk.Label(self.tabk, textvariable=aantal_label)
		label3.grid(row=val, column=3)
		print(self.aantallen[val])
		print(self.aantallen[:])

	def save(self):
		for i in range(len(self.d)):
			var=self.d[i,1].get()
			self.aantallen[i]=var
			aantal_label=StringVar()
			aantal_label.set(var)
			label3=tk.Label(self.tabk,textvariable=aantal_label)
			label3.grid(row=i, column=3)

	def next(self):
		#opslaan van data in excel file
		#opslaan op basis van numpy arrays
		ws.title = 'Bestelbon %s %s' % (self.input, '12_03_2017')
		ws['A1'] = 'Klant:'
		ws['B1'] = 'Huidige datum'
		ws['C1'] = 'Richtdatum'
		ws['A2'] = 'klant'
		ws['B2'] = datetime.datetime.now()
		ws['C2'] = 'datum afgewerkt'
		ws['A4'] = 'Producten'
		ws['C4'] = 'Aantal'
		ws['B'+str(len(self.producten)+5)]='Totaal'
		sum=0
		for i in range(len(self.producten)):
			ws['A' + str(i + 5)] = self.producten[i]
			ws['C' + str(i + 5)] = self.aantallen[i]
			sum+=self.aantallen[i]*self.prijzen[i]

		ws['C'+str(len(self.producten)+5)]=sum
		wb.save('Bestelbon %s %s.xlsx' % (self.input, '12_03_2017'))


			
			

app = Application(master=root)
app.mainloop()			
			
			
		
